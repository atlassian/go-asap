module bitbucket.org/atlassian/go-asap/v2

go 1.14

require (
	github.com/SermoDigital/jose v0.9.2-0.20161205224733-f6df55f235c2
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/google/uuid v1.3.0
	github.com/pquerna/cachecontrol v0.1.0
	github.com/stretchr/testify v1.6.1
	github.com/vincent-petithory/dataurl v1.0.0
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
)
