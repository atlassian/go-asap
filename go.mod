module bitbucket.org/atlassian/go-asap

go 1.14

require (
	github.com/SermoDigital/jose v0.9.2-0.20161205224733-f6df55f235c2
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/google/uuid v1.1.2
	github.com/pquerna/cachecontrol v0.0.0-20201205024021-ac21108117ac
	github.com/stretchr/testify v1.6.1
	github.com/vincent-petithory/dataurl v0.0.0-20160330182126-9a301d65acbb
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
)
